#!/usr/bin/bash

JOB="build-backend-code-job"
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"

TEXT="
Ostap S. build backend

Артефакт:
$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_SHA/download?job=$JOB
"

curl --location $URL \
    -H "Content-type: application/json" \
    -d "{\"chat_id\": -1001810571268, \"text\": \"$TEXT\"}"

