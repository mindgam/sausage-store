#!/bin/bash
#чтобы скрипт завершался, если есть ошибки
set -xe
#скачиваем артефакт
sudo cp -rf sausage-store-backend.service /etc/systemd/system/sausage-store-backend.service
sudo rm -rf /home/student/sausage-store.jar

curl -u ${NEXUS_REPO_USER}:${NEXUS_REPO_PASS} -o /home/student/sausage-store.jar ${NEXUS_REPO_URL}/${NEXUS_REPO_BACKEND_NAME}/com/yandex/practicum/devops/sausage-store/${VERSION}/sausage-store-${VERSION}.jar
sudo cp -rf /home/student/sausage-store.jar /opt/sausage-store/bin/sausage-store.jar||true #"<...>||true" говорит, если команда обвалится — продолжай
#Обновляем конфиг systemd с помощью рестарта
sudo systemctl daemon-reload
sudo systemctl enable sausage-store-backend
#Перезапускаем сервис сосисочной
sudo systemctl restart sausage-store-backend 