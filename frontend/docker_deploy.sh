#!/bin/bash
set -xe
sudo docker login -u ${CI_REGISTRY_USER} -p${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
sudo docker network create -d bridge sausage_network || true
sudo docker rm -f sausage-frontend || true

# Check sausage-backend
while ! sudo docker top sausage-backend &>/dev/null; do
    echo "Waiting for sausage-backend container to be available..."
    sleep 1
done

sudo docker run --restart=on-failure:10 -p 8080:80 \
    -v /home/${DEV_USER}/default.conf:/etc/nginx/conf.d/default.conf \
    -d --name sausage-frontend \
    --network=sausage_network \
    "${CI_REGISTRY_IMAGE}"/sausage-frontend:latest