#!/bin/bash
#чтобы скрипт завершался, если есть ошибки
set -xe

# nginx
if ! dpkg -l nginx &> /dev/null; then
    sudo apt update
    sudo apt install nginx -y
else
    echo "Nginx is already installed."
fi

sudo cp -rf sausage-store.conf /etc/nginx/sites-enabled/sausage-store.conf
sudo rm -rf /home/front-user/sausage-store-front.tar.gz

#скачиваем артефакт
curl -u ${NEXUS_REPO_USER}:${NEXUS_REPO_PASS} -o sausage-store-frontend.tar.gz ${NEXUS_REPO_URL}/${NEXUS_REPO_FRONTEND_NAME}/${VERSION}/sausage-store-${VERSION}.tar.gz


sudo tar -xvf ./sausage-store-frontend.tar.gz -C /var/www-data
sudo chown -R www-data:www-data /var/www-data


sudo systemctl daemon-reload
sudo systemctl enable nginx
sudo systemctl restart nginx
