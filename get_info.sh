#!/usr/bin/env bash

# Show help
show_help() {
    echo "Использование: get_info.sh [options]"
    echo "Options:"
    echo " --host        Show info about host"
    echo " --user        Show info about users"
    echo " --help        Show help"
}

# Output help if no value is passed
if [[ $# -eq 0 ]]; then
	show_help
    exit 0
fi

# 
print_host_info() {
    # Number of CPU cores
    cpu_cores=$(nproc)
    echo "Number of CPU cores: $cpu_cores"

    # Amount and utilization of RAM
    mem_info=$(awk '/^MemTotal/ { mem_total=$2 } /^MemAvailable/ { mem_available=$2 } END { printf "Memory total: %.2f GB\nMemory used: %.2f GB\n", mem_total/1024/1024, (mem_total-mem_available)/1024/1024 }' /proc/meminfo)
    echo "$mem_info"

    # Disk information
    disk_info=$(df -h --output=size,avail -x tmpfs -x devtmpfs)
    echo "Disk information:"
    echo "$disk_info"

    # Average system utilization
    load_avg=$(cat /proc/loadavg)
    echo "Load average: $load_avg"

    # Current time
    current_time=$(date +"%Y-%m-%d %H:%M:%S")
    echo "Current time: $current_time"

    # System uptime
    uptime_seconds=$(cat /proc/uptime | awk '{print $1}' | cut -d '.' -f 1)
    uptime_hours=$((uptime_seconds / 3600))
    echo "Uptime: $uptime_hours hours"

   # Displaying information about network interfaces
   echo "Network information:"
   interfaces=$(ip -o link show | awk -F': ' '{print $2}')
    while read -r interface; do
        interface_status=$(ip -o link show "$interface" | awk '{print $9}')
        interface_ip=$(ip addr show "$interface" | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
        interface_rx=$(cat /proc/net/dev | grep "$interface" | awk '{print $2}')
        interface_tx=$(cat /proc/net/dev | grep "$interface" | awk '{print $10}')
        interface_errors=$(cat /proc/net/dev | grep "$interface" | awk '{print $4}')
        echo "Interface $interface: status: $interface_status, IP: $interface_ip, RX: $interface_rx, TX: $interface_tx, errors: $interface_errors"
    done <<< "$interfaces"
   
    # List of listened ports
    listening_ports=$(netstat -tuln | awk '/^tcp/ || /^udp/ { printf "%s:%s\n", $4, $NF }')
    echo "Listening ports:"
    echo "$listening_ports"
}

print_user_info() {
    # List of users in the system
    all_users=$(awk -F':' '/\/home/ { printf "%s\n", $1 }' /etc/passwd)
    echo "All users:"
    echo "$all_users"

    # List of root users on the system
    root_users=$(awk -F':' '$3 == 0 { printf "%s\n", $1 }' /etc/passwd)
    echo "Root users:"
    echo "$root_users"

    # List of logged in users
    logged_in_users=$(who | cut -d' ' -f1)
    echo "Logged in users:"
    echo "$logged_in_users"
}

# Declare parameters 
OPTIONS=$(getopt -o hu --long help,host,user -- "$@")
eval set -- "$OPTIONS"

# Arguments handling
while true; do
    case "$1" in
        -h | --help)
        show_help
        exit 0 ;;
        -u | --user)
        print_user_info
        exit 0 ;;
        --host)
        print_host_info
        exit 0 ;;
        --)
        shift
        break ;;
        *)
        echo "Invalid option: $1"
        show_help
        exit 1 ;;
    esac
done


